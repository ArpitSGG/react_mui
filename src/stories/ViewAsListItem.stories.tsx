import { ComponentStory, ComponentMeta } from "@storybook/react";

import ViewAsListItem from "../components/ViewAsListItem";

export default {
  title: "ViewAsListItem",
  component: ViewAsListItem,
} as ComponentMeta<typeof ViewAsListItem>;

const Template: ComponentStory<typeof ViewAsListItem> = (args) => <ViewAsListItem {...args} />;

export const Component: ComponentStory<typeof ViewAsListItem> = Template.bind({});

Component.args = {
  details: {
    name: "John Doe",
    email: "johndoe@gmail.com",
    profilePic: "https://randomuser.me/api/portraits/",
    company: "Google",
    phone: "123456789",
    address: "123, Main Street, New York, NY, USA",
    countryCode: "US",
    language: "english",
    timezone: "America/New_York",
  },
};
