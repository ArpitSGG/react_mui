import { ComponentStory, ComponentMeta } from "@storybook/react";

import DetailDrawer from "../components/DetailDrawer";

export default {
  title: "DetailDrawer",
  component: DetailDrawer,
} as ComponentMeta<typeof DetailDrawer>;

const Template: ComponentStory<typeof DetailDrawer> = (args) => <DetailDrawer {...args} />;

export const Primary: ComponentStory<typeof DetailDrawer> = Template.bind({});

Primary.args = {
  open: true,
  details: {
    name: "Tushar Agrawal",
    company: "Tushar Agrawal",
    email: "Tushar Agrawal",
    countryCode: "91",
    address: "Tushar Agrawal",
    phone: "8989614001",
    language: "english",
    timezone: "Asia/Kolkata",
    profilePic: "https://picsum.photos/200",
  },
};
