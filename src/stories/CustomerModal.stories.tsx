import { ComponentStory, ComponentMeta } from "@storybook/react";

import CustomerModal from "../components/CustomerModal";

export default {
  title: "CustomerModal",
  component: CustomerModal,
} as ComponentMeta<typeof CustomerModal>;

const Template: ComponentStory<typeof CustomerModal> = (args) => <CustomerModal {...args} />;

export const NewUser: ComponentStory<typeof CustomerModal> = Template.bind({});
export const UpdateUser: ComponentStory<typeof CustomerModal> = Template.bind({});

NewUser.args = {
  open: true,
  type: "new",
  activeCustomer: {
    name: "Tushar Agrawal",
    company: "Tushar Agrawal",
    email: "Tushar Agrawal",
    countryCode: "91",
    address: "Tushar Agrawal",
    phone: "8989614001",
    language: "english",
    timezone: "Asia/Kolkata",
  },
};

UpdateUser.args = {
  open: true,
  type: "update",
  activeCustomer: {
    name: "Tushar Agrawal",
    company: "Tushar Agrawal",
    email: "Tushar Agrawal",
    countryCode: "91",
    address: "Tushar Agrawal",
    phone: "8989614001",
    language: "english",
    timezone: "Asia/Kolkata",
  },
};
