import { ComponentStory, ComponentMeta } from "@storybook/react";

import DeleteModal from "../components/DeleteModal";

export default {
  title: "DeleteModal",
  component: DeleteModal,
} as ComponentMeta<typeof DeleteModal>;

const Template: ComponentStory<typeof DeleteModal> = (args) => <DeleteModal {...args} />;

export const Primary: ComponentStory<typeof DeleteModal> = Template.bind({});

Primary.args = {
  open: true,
};
