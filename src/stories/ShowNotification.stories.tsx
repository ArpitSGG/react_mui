import { ComponentStory, ComponentMeta } from "@storybook/react";

import ShowNotification from "../components/ShowNotification";

export default {
  title: "ShowNotification",
  component: ShowNotification,
} as ComponentMeta<typeof ShowNotification>;

const Template: ComponentStory<typeof ShowNotification> = (args) => <ShowNotification {...args} />;

export const Component: ComponentStory<typeof ShowNotification> = Template.bind({});

Component.args = {
  message: "Hello",
};
