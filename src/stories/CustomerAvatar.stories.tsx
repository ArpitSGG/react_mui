import { ComponentStory, ComponentMeta } from "@storybook/react";

import CustomerAvatar from "../components/CustomerAvatar";

export default {
  title: "CustomerAvatar",
  component: CustomerAvatar,
} as ComponentMeta<typeof CustomerAvatar>;

const Template: ComponentStory<typeof CustomerAvatar> = (args) => <CustomerAvatar {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  alt: "Tushar Agrawal",
  src: "https://avatars0.githubusercontent.com/u/16098981?s=460&u=f9f8b8d8f9f8b8d8f9f8b8d8f9f8b8d8f9f8b8d&v=4",
};
