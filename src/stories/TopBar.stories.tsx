import { ComponentStory, ComponentMeta } from "@storybook/react";

import TopBar from "../components/TopBar";

export default {
  title: "TopBar",
  component: TopBar,
} as ComponentMeta<typeof TopBar>;

const Template: ComponentStory<typeof TopBar> = (args) => <TopBar {...args} />;

export const Component: ComponentStory<typeof TopBar> = Template.bind({});

Component.args = {
  title: "Hello",
};
