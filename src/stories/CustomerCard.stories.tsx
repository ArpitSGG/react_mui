import { ComponentStory, ComponentMeta } from "@storybook/react";

import CustomerCard from "../components/CustomerCard";

import { SelectChangeEvent } from "@mui/material";
export default {
  title: "CustomerCard",
  component: CustomerCard,
} as ComponentMeta<typeof CustomerCard>;

const Template: ComponentStory<typeof CustomerCard> = (args) => <CustomerCard {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  name: "Tushar Agrawal",
  company: "Tushar Agrawal",
  address: "Tushar Agrawal",
  phone: "8989614001",
  language: "english",
  timezone: "Asia/Kolkata",
  profilePic: "https://avatars0.githubusercontent.com/u/16098981?s=460&u=f9f8b8d8f9f8b8d8f9f8b8d8f9f8b8d8f9f8b8d&v=4",
};
