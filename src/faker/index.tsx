import { faker } from "@faker-js/faker";

import type { Customer } from "../App";

const generateFakeCustomers = (length: number = 100): Customer[] => {
  const customers = [];

  for (let i = 0; i < 100; i++) {
    customers.push({
      name: faker.name.findName(),
      email: faker.internet.email(),
      phone: faker.phone.phoneNumber(),
      countryCode: faker.address.countryCode(),
      company: "",
      address: faker.address.streetAddress(),
      language: ["english", "french", "spanish", "german", "hindi"][Math.floor(Math.random() * 3)],
      timezone: ["Asia/Kolkata", "Europe/London", "America/New_York", "Asia/Baku"][Math.floor(Math.random() * 4)],
      profilePic: faker.image.avatar(),
    });
  }

  return customers;
};

export default generateFakeCustomers;
