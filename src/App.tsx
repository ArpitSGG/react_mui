import { useState, useEffect } from "react";

import CssBaseline from "@mui/material/CssBaseline";

import TopBar from "./components/TopBar";
import DetailDrawer from "./components/DetailDrawer";
import MyList from "./components/MyList";
import DeleteModal from "./components/DeleteModal";
import ShowNotification from "./components/ShowNotification";

import generateFakeCustomers from "./faker";
import CustomerModal from "./components/CustomerModal";

import ServiceCollapse from "./components/ServiceCollapse";

export type Customer = {
  name: string;
  email: string;
  phone: string;
  profilePic?: string;

  countryCode: string;
  company: string;
  address: string;
  language: string;
  timezone: string;
};

const ar: Customer[] = generateFakeCustomers(210);

const App = () => {
  const [allCustomers, setAllCustomers] = useState<Customer[]>(ar);
  const [customers, setCustomers] = useState<Customer[]>(ar);
  const [drawerState, setDrawerState] = useState(false);
  const [activeCustomer, setActiveCustomer] = useState<Customer | undefined>(undefined);
  const [deleteState, setDeleteState] = useState(false);
  const [notification, setNotification] = useState("");
  const [customerModalState, setCustomerModalState] = useState<{ open: boolean; type: "new" | "update" }>({
    open: false,
    type: "new",
  });

  const [search, setSearch] = useState("");

  useEffect(() => {
    if (search === "") setCustomers(allCustomers);
    else {
      const filteredCustomers = ar.filter((customer) => {
        return customer.name.toLowerCase().includes(search.toLowerCase());
      });
      setCustomers(filteredCustomers);
    }
  }, [search, allCustomers]);

  const showUserDetails = (name: string) => {
    setDrawerState(true);
    setActiveCustomer(customers.find((customer) => customer.name === name));
  };

  const closeDrawer = () => {
    setDrawerState(false);
  };

  const showDeleteModal = () => {
    setDeleteState(true);
  };

  const hideDeleteModal = () => {
    setDeleteState(false);
  };

  const handleDelete = () => {
    const newCustomers = customers.filter((customer) => customer.name !== activeCustomer?.name);
    setAllCustomers(newCustomers);
    setCustomers(newCustomers);
    setDrawerState(false);
    setDeleteState(false);
    setNotification(`Customer deleted successfully!`);
    setActiveCustomer(undefined);
  };

  const handleCloseNotification = () => {
    setNotification("");
  };

  const showAddNewModal = () => {
    setCustomerModalState({
      open: true,
      type: "new",
    });
  };

  const showEditModal = () => {
    setCustomerModalState({
      open: true,
      type: "update",
    });
  };

  const closeCustomerModal = () => {
    setCustomerModalState({ open: false, type: "new" });
  };

  const addNewUser = (customer: Customer) => {
    const newCustomers = [customer, ...customers];
    setAllCustomers(newCustomers);
    setCustomers(newCustomers);
    setCustomerModalState({ open: false, type: "new" });
    setNotification(`Customer added successfully!`);
  };

  const updateUser = (customer: Customer) => {
    setActiveCustomer(customer);
    const newCustomers = customers.map((c) => (c.name === customer.name ? customer : c));
    setAllCustomers(newCustomers);
    setCustomers(newCustomers);
    setCustomerModalState({ open: false, type: "new" });
    setNotification(`Customer updated successfully!`);
  };

  const handleSetSearch = (search: string) => {
    setSearch(search);
  };

  return <ServiceCollapse />;

  return (
    <>
      <CssBaseline />
      <DetailDrawer
        open={drawerState}
        closeDrawer={closeDrawer}
        details={activeCustomer}
        showDeleteModal={showDeleteModal}
        showEditModal={showEditModal}
        updateUser={updateUser}
      />

      <TopBar title="Customers" iconButtonOnClick={showAddNewModal} setSearchDebounce={handleSetSearch} />
      <MyList customers={customers} onItemClick={showUserDetails} />
      <DeleteModal open={deleteState} closeModal={hideDeleteModal} deleteItem={handleDelete} />
      <ShowNotification message={notification} handleClose={handleCloseNotification} />
      <CustomerModal
        {...customerModalState}
        activeCustomer={activeCustomer}
        closeModal={closeCustomerModal}
        onClickSave={customerModalState.type === "new" ? addNewUser : updateUser}
      />
    </>
  );
};

export default App;
