import React, { useState } from "react";
import { useDebouncedCallback } from "use-debounce";

import Typography from "@mui/material/Typography";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/material/Box";

import OutlinedInput from "@mui/material/OutlinedInput";

import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";

import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import SearchIcon from "@mui/icons-material/Search";

type Props = {
  title: string;
  iconButtonOnClick?: () => void;
  setSearchDebounce: (search: string) => void;
};

const AppBarHOC = ({ children }: { children: React.ReactElement }) => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
};

const TopBar = (props: Props) => {
  const [search, setSearch] = useState("");

  const debounced = useDebouncedCallback(
    // function
    (value) => {
      props.setSearchDebounce(value);
    },
    // delay in ms
    1000
  );

  const handleChangeText = (e: React.ChangeEvent<HTMLInputElement>) => {
    debounced(e.target.value);
    setSearch(e.target.value);
  };

  return (
    <>
      <AppBarHOC>
        <AppBar
          position="fixed"
          sx={{
            backgroundColor: "background.paper",
            px: 2,
          }}>
          <Toolbar>
            <Typography
              variant="h5"
              component="div"
              color="black"
              sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}>
              {props.title}
            </Typography>
            <Box alignItems="center" justifyContent="center" display="flex">
              <OutlinedInput
                startAdornment={
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                }
                placeholder="Search"
                size="small"
                value={search}
                onChange={handleChangeText}
                sx={{ borderRadius: 60, mx: 2 }}
              />
              <IconButton onClick={props.iconButtonOnClick} sx={{ backgroundColor: "grey.200" }}>
                <AddOutlinedIcon />
              </IconButton>
            </Box>
          </Toolbar>
        </AppBar>
      </AppBarHOC>
      <Toolbar />
    </>
  );
};

export default TopBar;
