import Drawer from "@mui/material/Drawer";

import Box from "@mui/material/Box";

import IconButton from "@mui/material/IconButton";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import ModeEditOutlineOutlinedIcon from "@mui/icons-material/ModeEditOutlineOutlined";
import CloseOutlinedIcon from "@mui/icons-material/CloseOutlined";
import Card from "@mui/material/Card";

import type { Customer } from "../../App";
import { Typography } from "@mui/material";
import { SelectChangeEvent } from "@mui/material/Select";

import KeyValuePair from "../KeyValuePair";
import CustomerCard from "../CustomerCard";
import ViewAsListItem from "../ViewAsListItem";

type Props = {
  open: boolean;
  details?: Customer;
  closeDrawer: () => void;
  showDeleteModal: () => void;
  showEditModal: () => void;
  updateUser: (customer: Customer) => void;
};

const DetailDrawer = (props: Props) => {
  const handleTimezoneChange = (event: SelectChangeEvent) => {
    const { details } = props;
    if (details) {
      const { value } = event.target;
      props.updateUser({ ...details, timezone: value });
    }
  };
  const handleLanguageChange = (event: SelectChangeEvent) => {
    const { details } = props;
    if (details) {
      const { value } = event.target;
      props.updateUser({ ...details, language: value });
    }
  };

  return (
    <Drawer anchor="right" open={props.open}>
      <Box sx={{ display: "flex", justifyContent: "flex-end", m: 2, minWidth: "900px" }}>
        <IconButton onClick={props.showEditModal}>
          <ModeEditOutlineOutlinedIcon />
        </IconButton>

        <IconButton onClick={props.showDeleteModal}>
          <DeleteOutlinedIcon />
        </IconButton>

        <IconButton onClick={props.closeDrawer}>
          <CloseOutlinedIcon />
        </IconButton>
      </Box>
      <Box
        sx={{
          px: 2,
        }}>
        <ViewAsListItem details={props.details} />
        <Box
          sx={{
            mt: 7,
          }}>
          <Typography variant="caption" color="text.secondary">
            DETAILS
          </Typography>
          <CustomerCard
            {...props.details}
            onTimezoneChange={handleTimezoneChange}
            onLanguageChange={handleLanguageChange}
          />
        </Box>
        <Box
          sx={{
            mt: 7,
          }}>
          <Typography variant="caption" color="text.secondary">
            SPECIAL DATES
          </Typography>
          <Card
            sx={{
              px: 4,

              my: 1,
            }}>
            <KeyValuePair title="Birth Date"></KeyValuePair>
          </Card>
        </Box>
      </Box>
    </Drawer>
  );
};

export default DetailDrawer;
