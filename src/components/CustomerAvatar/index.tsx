import Avatar from "@mui/material/Avatar";

function stringToColor(string: string = "tushar") {
  if (!string) return "gray";

  let hash = 0;
  let i;

  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = "#";

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }

  return color;
}

const CustomerAvatar = ({ alt, src }: { alt?: string; src: string }) => {
  if (!alt) <Avatar src={src} />;

  return (
    <Avatar
      alt={alt}
      src={src}
      sx={{
        backgroundColor: stringToColor(alt),
      }}>
      {alt && alt[0].toUpperCase()}
    </Avatar>
  );
};

export default CustomerAvatar;
