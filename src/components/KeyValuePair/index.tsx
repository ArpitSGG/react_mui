import React from "react";

import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

const index = ({ children, title }: { children?: React.ReactNode; title: string }) => {
  return (
    <Box sx={{ display: "flex", flexDirection: "row", flexWrap: "nowrap", my: 1 }}>
      <Box sx={{ width: "30%" }}>
        <Typography variant="caption" color="text.secondary">
          {title}
        </Typography>
      </Box>
      <Box sx={{ width: "70%", display: "flex", justifyContent: "flex-start" }}>{children}</Box>
    </Box>
  );
};

export default index;
