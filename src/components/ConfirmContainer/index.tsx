import Button from "@mui/material/Button";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
const ConfirmContainer = () => {
  return (
    <Dialog open={true} fullWidth={true}>
      <DialogTitle>Delete Service</DialogTitle>
      <Typography sx={{ mx: 3, my: 1 }} color="text.secondary">
        Are you sure you want to continue?
      </Typography>
      <Stack direction="row" justifyContent="flex-end" alignItems="center" spacing={2} sx={{ m: 1, marginTop: 2 }}>
        <Button sx={{ color: "black" }}>cancel</Button>
        <Button variant="contained">confirm</Button>
      </Stack>
    </Dialog>
  );
};

export default ConfirmContainer;
