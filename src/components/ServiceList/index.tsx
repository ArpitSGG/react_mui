import { Modal, Stack, Typography, Box } from "@mui/material";

import List from "@mui/material/List";

import ServiceListItem from "../ServiceListItem";

import data from "./data";
const modalStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  height: "70vh",
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

const ServiceList = () => {
  return (
    <Modal open={true} onClose={() => {}}>
      <Box sx={modalStyle}>
        <Typography sx={{ mx: 9 }} textTransform="uppercase" color="text.secondary">
          Select Service
        </Typography>
        <List sx={{ my: 3 }}>
          {data.map((service, index, arr) => (
            <ServiceListItem key={service.id} data={service} divider={arr.length - 1 > index} />
          ))}
        </List>
      </Box>
    </Modal>
  );
};

export default ServiceList;
