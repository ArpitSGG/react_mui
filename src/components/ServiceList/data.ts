export type Service = {
    id: number;
    service: string;
    price: string;
    duration: string;
    color: string;
    features: string[];
    requiresPayment: boolean;

}



const data: Service[]= [
    {
        id: 1,
        service: "Haircut",
        price: "₹799.00",
        color: "#9D69AF",
        "duration": "30m",
        features: [],
        requiresPayment: false
    }
    ,
    {
        id: 2,
        service: "Hair Color",
        color: "#EF6C00",
        price: "₹1299.00",
        "duration": "45m",
        features: [],
        requiresPayment: false


    },
    {
        id: 3,
        service: "Hair Spa",
        price: "₹1599.00",
        color: "#D50101",
        "duration": "120m",
        features: ["Group service"],
        requiresPayment: false


    }

]

export default data;