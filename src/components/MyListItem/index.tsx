import ListItemButton from "@mui/material/ListItemButton";
import ViewAsListItem from "../ViewAsListItem";

import type { Customer } from "../../App";

type Props = Customer & {
  onClick?: (name: string) => void;
};

const index = (props: Props) => {
  const handleOnClick = () => {
    if (props.onClick) {
      props.onClick(props.name);
    }
  };

  return (
    <ListItemButton onClick={handleOnClick}>
      <ViewAsListItem details={props} />
    </ListItemButton>
  );
};

export default index;
