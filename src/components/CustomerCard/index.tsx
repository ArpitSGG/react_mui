import React from "react";

import Card from "@mui/material/Card";
import type { Customer } from "../../App";

import KeyValuePair from "../KeyValuePair";

import FormControl from "@mui/material/FormControl";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";

type Props = {
  onLanguageChange?: (event: SelectChangeEvent) => void;
  onTimezoneChange?: (event: SelectChangeEvent) => void;
} & Customer;

const CustomerCard = (props: Partial<Props>) => {
  return (
    <Card
      sx={{
        px: 4,
        my: 1,
      }}>
      <KeyValuePair title="Company">{props.company}</KeyValuePair>
      <KeyValuePair title="Address">{props.address}</KeyValuePair>
      <KeyValuePair title="Telephones">{props.phone}</KeyValuePair>
      <KeyValuePair title="Language">
        <FormControl sx={{ m: 1, minWidth: 120 }} size="small" variant="standard">
          <Select value={props.language} sx={{ textTransform: "capitalize" }} onChange={props.onLanguageChange}>
            {["english", "french", "spanish", "german", "hindi"].map((lang) => (
              <MenuItem key={lang} value={lang} sx={{ textTransform: "capitalize" }}>
                {lang}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </KeyValuePair>
      <KeyValuePair title="Timezone">
        <FormControl sx={{ m: 1, minWidth: 120 }} size="small" variant="standard">
          <Select value={props.timezone} onChange={props.onTimezoneChange}>
            {["Asia/Kolkata", "Europe/London", "America/New_York", "Asia/Baku"].map((zone) => (
              <MenuItem key={zone} value={zone}>
                {zone}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </KeyValuePair>
    </Card>
  );
};

export default CustomerCard;
