export type Service = {
    id: number;
    service: string;
    price: string;
    duration: string;
    color: string;
    features: string[];
    requiresPayment: boolean;
}

export type Meeting = {
    
        id: string;
        title: string;
        services: Service[];
    color: string;
    
}

const data: Meeting[] = [
    {
        id: 'm1',
        color: '#347EDD',
        title: 'Bridal Services',
        services: [
            {
                id: 1,
                service: "Haircut",
                price: "Starting from ₹999",
                color: "#9D69AF",
                "duration": "30m-60m",
                features: ["Served at customer location", "Group service"],
                requiresPayment: true
            }
            ,
            {
                id: 2,
                service: "Hair Color",
                color: "#EF6C00",
                price: "₹1299.00",
                "duration": "45m",
                features: ["Online service"],
                requiresPayment: true
     
            },
            {
                id: 3,
                service: "Hair Spa",
                price: "₹1599.00",
                color: "#D50101",
                "duration": "120m",
                features: ["At business location"],
                requiresPayment: true

        
            }
        ]

    },
     {
        id: 'm2',
        title: 'Default',
        color: '#fff',
        services: []
     }
]

export default data;