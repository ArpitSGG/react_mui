import { Button, Modal, Stack, Typography, Box } from "@mui/material";

import List from "@mui/material/List";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Avatar from "@mui/material/Avatar";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import data from "./data";

import ServiceListItem from "../ServiceListItem";

const modalStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  height: "70vh",
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

const ServiceCollapse = () => {
  return (
    <Modal open={true}>
      <Box sx={modalStyle}>
        <Typography sx={{ mx: 9 }} textTransform="uppercase" color="text.secondary">
          Select Meeting Type
        </Typography>
        {data.map((meeting) => (
          <Accordion key={meeting.id} sx={{ boxShadow: 0, border: "none", marginBottom: 0 }}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Stack alignItems="center" direction="row">
                <Avatar sx={{ bgcolor: meeting.color }}>{meeting.title[0]}</Avatar>
                <Typography mx={3} textTransform="uppercase" color="text.secondary">
                  {meeting.title}
                </Typography>
              </Stack>
            </AccordionSummary>
            <AccordionDetails sx={{ p: 0, m: 0 }}>
              <List sx={{ p: 0 }}>
                {meeting.services.map((service, index, arr) => (
                  <ServiceListItem key={service.id} data={service} divider={arr.length - 1 > index} />
                ))}
              </List>
            </AccordionDetails>
          </Accordion>
        ))}
      </Box>
    </Modal>
  );
};

export default ServiceCollapse;
