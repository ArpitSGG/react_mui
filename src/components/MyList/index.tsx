import List from "@mui/material/List";
import { Typography } from "@mui/material";
import MyListItem from "../MyListItem";

import type { Customer } from "../../App";
import { Divider } from "@mui/material";
import { Box } from "@mui/system";

const MyList = ({ customers, onItemClick }: { customers: Customer[]; onItemClick: (name: string) => void }) => {
  if (customers.length === 0)
    return (
      <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
        <Typography sx={{ mx: 4, my: 2, opacity: 0.7 }} variant="h4" color="text.secondary">
          No Customers Found
        </Typography>
      </Box>
    );

  return (
    <List sx={{ px: 4 }}>
      {customers.map((customer, index) => {
        const component = <MyListItem {...customer} key={`customer-${index}`} onClick={onItemClick} />;
        return (
          <Box key={customer.name}>
            {component} {index < customers.length - 1 ? <Divider key={`divider-${index}`} /> : null}
          </Box>
        );
      })}
    </List>
  );
};

export default MyList;
