import { Button, Stack, Typography, Box, Divider, Grid } from "@mui/material";

import ListItem from "@mui/material/ListItem";
import Avatar from "@mui/material/Avatar";
import CreditCardOutlinedIcon from "@mui/icons-material/CreditCardOutlined";
import type { Service } from "../ServiceCollapse/data";

const ServiceListItem = ({ data, divider }: { data: Service; divider: boolean }) => {
  return (
    <>
      <ListItem
        sx={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
          "& button": {
            visibility: "hidden",
          },
          "&:hover": {
            backgroundColor: "grey.100",
          },
          "&:hover button": {
            visibility: "visible",
          },
        }}>
        <Stack direction="row" alignItems="center">
          <Avatar variant="square" sx={{ borderRadius: 1, bgcolor: data.color }}>
            {data.service[0]}
          </Avatar>
          <Stack direction="column" sx={{ mx: 2, color: "text.secondary" }} alignItems="flex-start">
            <Typography variant="body1">{data.service}</Typography>
            <Grid container spacing={1}>
              <Grid item>
                <Typography variant="body2">{data.duration}</Typography>
              </Grid>
              <DotSeparator />
              <Grid item>
                <Typography variant="body2">{data.price}</Typography>
              </Grid>
              {data.features.map((feature, index) => (
                <>
                  <DotSeparator key={"d" + index} />
                  <Grid item key={index}>
                    <Typography variant="body2">{feature}</Typography>
                  </Grid>
                </>
              ))}
            </Grid>
            {data.requiresPayment && (
              <Stack direction="row" alignItems="center" marginTop={2}>
                <CreditCardOutlinedIcon />
                <Typography mx={1}>Requires pre-payment</Typography>
              </Stack>
            )}
          </Stack>
        </Stack>
        <Button variant="outlined">Select</Button>
      </ListItem>
      {divider && <Divider />}
    </>
  );
};

export default ServiceListItem;

const DotSeparator = () => {
  return (
    <Grid item my={1}>
      <Box
        sx={{
          width: 6,
          height: 6,
          borderRadius: "50%",
          bgcolor: "text.secondary",
        }}
      />
    </Grid>
  );
};
