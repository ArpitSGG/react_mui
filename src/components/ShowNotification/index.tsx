import Snackbar from "@mui/material/Snackbar";
import Button from "@mui/material/Button";

const index = ({ message, handleClose }: { message: string; handleClose: () => void }) => {
  return (
    <Snackbar
      open={message.length > 0}
      autoHideDuration={1000}
      ContentProps={{
        sx: {
          backgroundColor: "primary.main",
        },
      }}
      message={message}
      action={
        <Button size="small" onClick={handleClose} sx={{ color: "common.white" }}>
          Dismiss
        </Button>
      }
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
    />
  );
};

export default index;
