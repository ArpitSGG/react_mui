import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

import type { Customer } from "../../App";

import CustomerAvatar from "../CustomerAvatar";

const ViewAsListItem = ({ details }: { details?: Customer }) => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        my: 1,
      }}>
      <CustomerAvatar alt={details?.name} src={details?.profilePic ? details.profilePic : " "} />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          mx: 4,
        }}>
        <Typography>{details ? details.name : "No details"}</Typography>
        <Typography variant="body2" color="text.secondary">
          {details ? details.email : "No details"}
        </Typography>
      </Box>
    </Box>
  );
};

export default ViewAsListItem;
