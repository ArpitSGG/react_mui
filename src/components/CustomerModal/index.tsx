import { useState, useEffect } from "react";

import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import CloseOutlinedIcon from "@mui/icons-material/CloseOutlined";
import IconButton from "@mui/material/IconButton";

import type { Customer } from "../../App";

type Props = {
  open: boolean;
  closeModal: () => void;
  onClickSave: (customer: Customer) => void;
  type: "update" | "new";
  activeCustomer?: Customer | undefined;
};

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  borderRadius: 2,
  boxShadow: 24,
  p: 2,
};

const CustomerModal = ({ open, closeModal, onClickSave, type, activeCustomer }: Props) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");

  useEffect(() => {
    if (activeCustomer && type === "update") {
      setName(activeCustomer.name);
      setEmail(activeCustomer.email);
      setPhone(activeCustomer.phone);
    } else {
      setName("");
      setEmail("");
      setPhone("");
    }
  }, [type, activeCustomer]);

  const handleSuccessClick = () => {
    var customer: Customer;

    if (activeCustomer && type === "update") {
      customer = {
        ...activeCustomer,
        name,
        email,
        phone,
      };
    } else {
      customer = {
        profilePic: "",
        countryCode: "",
        company: "",
        address: "",
        language: "english",
        timezone: "Aisa/Kolkata",
        name,
        email,
        phone,
      };
    }

    onClickSave(customer);
  };

  return (
    <Modal
      open={open}
      onClose={closeModal}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
          }}>
          <IconButton onClick={closeModal}>
            <CloseOutlinedIcon />
          </IconButton>
        </Box>
        <Typography variant="h5" component="h2">
          {type === "new" ? "New" : "Update"} Customer
        </Typography>
        <Box sx={{ display: "flex", flexDirection: "column", my: 3 }}>
          <TextField
            variant="standard"
            placeholder="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            sx={{ my: 1 }}
          />
          <TextField
            variant="standard"
            placeholder="Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            sx={{ my: 1 }}
          />
          <TextField
            variant="standard"
            placeholder="Phone"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            sx={{ my: 1 }}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
          }}>
          <Button size="large" sx={{ mx: 2, color: "text.secondary" }} onClick={closeModal}>
            Cancel
          </Button>
          <Button variant="contained" size="large" onClick={handleSuccessClick}>
            Save
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default CustomerModal;
