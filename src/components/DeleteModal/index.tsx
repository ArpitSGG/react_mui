import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  borderRadius: 2,
  boxShadow: 24,
  p: 2,
};

const DeleteModal = ({
  open,
  closeModal,
  deleteItem,
}: {
  open: boolean;
  closeModal: () => void;
  deleteItem: () => void;
}) => {
  return (
    <Modal
      open={open}
      onClose={closeModal}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography variant="h5" component="h2">
          Delete Customer
        </Typography>
        <Typography sx={{ my: 3, color: "text.secondary" }}>Are you sure you want to continue?</Typography>
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
          }}>
          <Button size="large" sx={{ mx: 2, color: "text.secondary" }} onClick={closeModal}>
            Cancel
          </Button>
          <Button variant="contained" size="large" onClick={deleteItem}>
            Confirm
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default DeleteModal;
